#FROM fabric8/java-alpine-openjdk8-jdk as builder
FROM openjdk:8 as builder
WORKDIR /build

#Directories
COPY src/ ./src
COPY ./.mvn ./.mvn

#Files
COPY pom.xml .
COPY ./mvnw .
COPY ./mvnw.cmd .

#Run command
RUN ./mvnw package

FROM fabric8/java-alpine-openjdk8-jre
ENV JAVA_OPTIONS="-Dquarkus.http.host=0.0.0.0 -Djava.net.preferIPv4Stack=true -Djava.util.logging.manager=org.jboss.logmanager.LogManager"
ENV AB_ENABLED=jmx_exporter
COPY --from=builder /build/target/lib/* /deployments/lib/
COPY --from=builder /build/target/my-quarkus-project-1.0-SNAPSHOT-runner.jar /deployments/app.jar
ENTRYPOINT [ "/deployments/run-java.sh" ]
